const User = require('../models/user');
const sanitizeHtml = require('sanitize-html');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { uploadImage, deleteImage } = require('../utils/cloudinary');
const config = require('config');
const { jwt_secret } = config.get("jwt_config");

/**
 * Register a new User
 * @param req
 * @param res
 * @returns User
 */
registerUser = async (req, res) => {

  const { username, email, password } = req.body;

  if(!username || !email || !password) {
    return res.status(400).send({ msg: "Please enter all fields."});
  }

  User.findOne({ email }).exec((err, user) => {
    if (err) return res.status(500).send(err);
    if(user) return res.status(400).send({msg: "User already exists."})
    
    const newUser = new User({
      username,
      email,
      password
    });

    // Let's sanitize inputs
    newUser.username = sanitizeHtml(newUser.username);
    newUser.email = sanitizeHtml(newUser.email);
    newUser.password = sanitizeHtml(newUser.passwor);

    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(newUser.password, salt, (err, hash) => {
        if(err) throw err;
        newUser.password = hash;
        newUser.save()
        .then(user => {
          jwt.sign(
            { secure: user.id },
            jwt_secret,
            { expiresIn: 3600 },
            (err, token) => {
              if(err) throw err;
              res.status(200).send({
                token,
                user
              });
            }
          )
        })
        .catch(err => {
          res.status(500).send({msg: "Unexpected error registering user."})
        });
      })
    })
  });
};


/**
 * Login with a user
 * @param req
 * @param res
 * @returns user
 */
loginUser = async (req, res) => {
  const { email, password } = req.body;

  if(!email || !password) return res.status(400).send({ msg: "Please, enter all fields." })

  User.findOne({ email })
  .then(user => {
    if(!user) return res.status(400).send({ msg: "User does not exist." });

    bcrypt.compare(password, user.password)
    .then(isMatch => {
      if(!isMatch) return res.status(400).send({ msg: "Invalid credentials."});

      jwt.sign(
        { secure: user.id },
        jwt_secret,
        { expiresIn: 3600 },
        (err, token) => {
          if(err) throw err;
          res.status(200).send({
            token,
            user
          });
        }
      )
    })
  })
};


/**
 * Gets a specific user
 * @param req
 * @param res
 * @returns user
 */
getUser = async (req, res) => {
  User.findById(req.user.secure)
  .select('-password')
  .then(user => {
    res.status(200).send({ user });
  })
};

/**
 * Sets a new avatar for a user
 * @param req
 * @param res
 * @returns user
 */
setNewAvatar = async (req, res) => {
  if (!req.body.data.image) {
    return res.status(403).end();
  }
  User.findById(req.user.secure)
  .select('-password')
  .then(async user => {

    let cloudinaryResponse;

    // Delete previous avatar if the user had any:
    if(user.avatar) cloudinaryResponse = await deleteImage(user.avatar.publicId).catch(err => res.status(500).send(err));

    cloudinaryResponse = await uploadImage(req.body.data.image).catch(err => res.status(500).send(err));

    if(!cloudinaryResponse) return

    user.avatar = { url: cloudinaryResponse.url, publicId: cloudinaryResponse.public_id }
    
    user.save((err, user) => {
      if (err) {
        return res.status(500).send(err);
      }
      res.status(200).send({ user });
    });
  })
};

module.exports = {
  registerUser,
  loginUser,
  getUser,
  setNewAvatar
};
