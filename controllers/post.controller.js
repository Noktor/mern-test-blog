const Post = require('../models/post');
const cuid = require('cuid');
const slug = require('limax');
const sanitizeHtml = require('sanitize-html');
const { uploadImage, deleteImage } = require('../utils/cloudinary');

/**
 * Get all posts
 * @param req
 * @param res
 * @returns void
 */
getPosts = async (req, res) => {
  Post.find().sort('-dateAdded').exec((err, posts) => {
    if (err) {
      return res.status(500).send(err);
    }
    res.json({ posts });
  });
};

/**
 * Save a post
 * @param req
 * @param res
 * @returns void
 */
addPost = async (req, res) => {
  if (!req.body.post.name || !req.body.post.title || !req.body.post.content) {
    return res.status(403).end();
  }

  const newPost = new Post(req.body.post);

  // Let's sanitize inputs
  newPost.title = sanitizeHtml(newPost.title);
  newPost.name = sanitizeHtml(newPost.name);
  newPost.content = sanitizeHtml(newPost.content);

  newPost.slug = slug(newPost.title.toLowerCase(), { lowercase: true });
  newPost.cuid = cuid();
  newPost.save((err, saved) => {
    if (err) {
      return res.status(500).send(err);
    }
    res.json({ post: saved });
  });
};

/**
 * Add an image to a Post
 * @param req
 * @param res
 * @returns void
 */
addImage = async (req, res) => {
  if (!req.body.data.image || !req.body.data.cuid) {
    return res.status(403).end();
  }

  let cloudinaryResponse = await uploadImage(req.body.data.image).catch(err => res.status(500).send(err));

  if(!cloudinaryResponse) return

  let filter = { cuid: sanitizeHtml(req.body.data.cuid) };
  let update = { $push: { images: { url: cloudinaryResponse.url, publicId: cloudinaryResponse.public_id } } }

  Post.update(filter, update,
  ).exec((err, post) => {
    if (err) {
      return res.status(500).send(err);
    }
    res.status(200).send({msg: "ok", image: { url: cloudinaryResponse.url, publicId: cloudinaryResponse.public_id } });
  });
};

/**
 * Delete an image from a post
 * @param req
 * @param res
 * @returns void
 */
delImage = async (req, res) => {
  if (!req.params.cuid || !req.params.publicId) {
    res.status(403).end();
  }

  let cloudinaryResponse = await deleteImage(req.params.publicId).catch(err => res.status(500).send(err));

  if(!cloudinaryResponse) return

  Post.update(
  {
    cuid: req.params.cuid
  },
  {
    $pull:
    {
      images: {
        publicId: req.params.publicId
      }
    }
  },(err, post) => {
    if (err) {
      return res.status(500).send(err);
    }
    res.status(200).end();
  });
};

/**
 * Get a single post
 * @param req
 * @param res
 * @returns void
 */
getPost = async (req, res) => {
  Post.findOne({ cuid: req.params.cuid }).exec((err, post) => {
    if (err) {
      res.status(500).send(err);
    }
    Post.findOne({ cuid: req.params.cuid }).exec((err, post) => {
      if (err) {
        return res.status(500).send(err);
      }
      res.json({ post });
    });
  });
};

/**
 * Delete a post
 * @param req
 * @param res
 * @returns void
 */
deletePost = async (req, res) => {
  Post.findOne({ cuid: req.params.cuid }).exec(async (err, post) => {
    if (err) return res.status(500).send(err);

    // Small routine that deletes all images related to that post.
    for(let image of post.images) {
      await deleteImage(image.publicId).catch(err => res.status(500).send(err));
    }

    post.remove(() => {
      res.status(200).end();
    });
  });
};

module.exports = {
  getPosts,
  addPost,
  getPost,
  deletePost,
  addImage,
  delImage
};