const cloudinary = require('cloudinary').v2;
const config = require('config');
const {api_secret, cloud_name, api_key} = config.get('jwt_config');

cloudinary.config({ 
    cloud_name: cloud_name, 
    api_key: api_key, 
    api_secret: api_secret
});

function uploadImage(image){
    return new Promise((resolve, reject) => {
        cloudinary.uploader
        .upload(image)
        .then((result, error) => {
            resolve(result);
        })
        .catch(err => {
            reject(err);
        });
    })
}

function deleteImage(image){
    return new Promise((resolve, reject) => {
        cloudinary.uploader
        .destroy(image)
        .then((result, error) => {
            resolve(result);
        })
        .catch(err => {
            reject(err);
        });   
    })
}

module.exports = {
    uploadImage,
    deleteImage
}