const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ImageSchema = require('./image');

const userSchema = new Schema({
    username: { type: 'String', required: true },
    email: { type: 'String', required: true },
    password: { type: 'String', required: true },
    avatar: { type: ImageSchema, required: false }
});

module.exports = mongoose.model('User', userSchema);