const mongoose = require('mongoose');
const config = require('config');
const mongoURI = config.get('mongoURI');

mongoose
    .connect(mongoURI, { useNewUrlParser: true, useUnifiedTopology: true })
    .catch(e => {
        console.error('Connection error', e.message)
    });

const db = mongoose.connection;

module.exports = db;
