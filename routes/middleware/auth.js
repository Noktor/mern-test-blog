const jwt = require('jsonwebtoken');
const config = require('config');
const { jwt_secret } = config.get("jwt_config");

function auth(req, res, next) {
    const token = req.header('x-auth-token');

    if(!token) return res.status(401).send({ msg: "No token, authorization denied." })

    try{
        const decoded = jwt.verify(token, jwt_secret);
        req.user = decoded;
        next();
    } catch (err) {
        res.status(400).send({ msg: "Token is not valid." });
    }
}

module.exports = auth;