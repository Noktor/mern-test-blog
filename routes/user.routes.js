const express = require('express');
const router = express.Router();
const UserRoutes = require('../controllers/user.controller');
const auth = require('./middleware/auth');

// Register a new User
router.route('/users/register').post(UserRoutes.registerUser);

// Login a User
router.route('/users/login').post(UserRoutes.loginUser);

// Retrieve an existing User
router.route('/users/user').get( auth, UserRoutes.getUser);

// Sets a new avatar for an User
router.route('/users/avatar').post( auth, UserRoutes.setNewAvatar);

module.exports = router;